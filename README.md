# Docker postfix

Solution open source configurant un Postfix pour un nom de domaine paramétrable.
Postfix ouvre un serveur SMTP sur port 25.

***Attention à ne pas ouvrir ce port sur l'extérieur !!!***

# Installation d'un environnement de développement

## Premier lancement
Une fois le projet récupéré, le lancement de l'application se réalise simplement à la racine du projet par :
```
$> git clone ssh://git.beta.pole-emploi.fr:23/open-source/toolkit-oss/dockers/postfix.git
$> cd postfix
postfix$> docker-compose up -d --build
```
Le projet peut se paramétrer par le biais du fichier ".env" lu par docker-compose.

Pour l'installation de docker et docker-compose, ***suivre les prérequis plus bas.***

## Paramétrage

Un fichier ".env" peut être créé à la racine du projet, pour paramétrer le build docker-compose avec les éléments suivants:

Paramètre       | Options                                         | Defaut                   |
----------------|:------------------------------------------------|--------------------------|
MAIL_DOMAIN     | Domaine d'envoi du mail ex: pole-emploi.fr      | domain.tld               |
DKIM_SELECTOR   | Selecteur DKIM                                  | mail                     |
DKIM_PRIVATEKEY | Clef DKIM privée                                | (vide)                   |
MAIL_NETWORK    | Networks autorisés à se connecter au postfix    | 0.0.0.0 (tous)           |
MAIL_SLOWDOWN   | Domaines dont il faut ralentir l'envoi en masse | orange.fr wanadoo.fr ... |
TIMEZONE        | Timezone du postfix                             | Europe/Paris             |

## Le DKIM

Par défaut, si aucune clef DKIM privée n'est fournie dans la variable DKIM_PRIVATEKEY, le dockerfile génère un jeu de clefs privée/publique dans le conteneur: /etc/dkimkeys/keys/domain.tld/ (domain.tld doit être changé par votre domaine d'envoi).

Attention à rendre persistent ce répertoire dans le docker-compose.yml. Le risque est de voir regéner un jeu de clefs à chaque execution.

La clef privée générée est à positionner sur le champ TXT (DKIM) du domaine sur le DNS.

Exemple de contenu de la clef publique. `cat mail.txt`:
```
mail._domainkey	IN	TXT	( "v=DKIM1; h=sha256; k=rsa; "  "p=MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAuSAvUPZnUMZ4lFuuBQEz5wvtjx7V3uElJV5GPYfHF1z0d2Lw0peZ3WEXSLreGdsm61vP8..." )  ; ----- DKIM key mail for domain.tld
```

Dans cet exemple, le champ TXT DKIM du domaine `mail._domainkey.domain.tld` doit donc posséder la valeur:
```
v=DKIM1; h=sha256; k=rsa; p=MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAuSAvUPZnUMZ4lFuuBQEz5wvtjx7V3uElJV5GPYfHF1z0d2Lw0peZ3WEXSLreGdsm61vP8...
```

## Le champ SPF

Ce champ DNS (TXT) soit posséder l'IP publique du serveur Postfix.

Exemple:
```
domain.tld	text = "v=spf1 a mx ip4:<ip publique> ~all"
```

## Les autres configurations DNS

Utiliser le service web [Mail Tester](https://www.mail-tester.com/) qui donne des conseils sur la configuration de vos mails et des DNS (comme le champ DMARC).

## Prérequis : installer docker et docker-compose

### Docker pour Ubuntu & Debian

```
$> sudo apt-get -y install apt-transport-https ca-certificates curl software-properties-common
$> sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
$> sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/$(lsb_release -is | tr '[:upper:]' '[:lower:]') $(lsb_release -cs) stable"
$> sudo apt-get update
$> sudo apt-get install -y docker-ce
$> sudo usermod -aG docker $USER
```

### Docker-compose (nécéssite Python)

```
$> sudo curl -L https://github.com/docker/compose/releases/download/1.25.0-rc2/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
$> sudo chmod +x /usr/local/bin/docker-compose
```
